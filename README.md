# GO-PSCAN

Scans ports.
Grabs banners.
What more could you want.

### BUILD
```$ go build portscanner.go```

### USE
```$ portscanner \<target IP> \<# Threads to use> \<Start Port> \<End Port> \<Timeout (Seconds)>```